import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, Subscription, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  url="http://localhost:3000/posts";
  private refreshServiceNeeded = new Subject<void>();
 
  constructor(private http: HttpClient) { }
 
  get refreshNeeded() {
      return this.refreshServiceNeeded;
  }
  public getData(): Observable<any> {
    return this.http.get<any>(this.url, httpOptions).pipe(
        retry(3)
    );
}

public postData(payload: Object): Observable<any> {
  return this.http.post(this.url, payload, httpOptions).pipe(
    retry(2),
    tap(() => {
      this.refreshServiceNeeded.next();
  })
  );
}

public removeData(id: string): Observable<any> {
  return this.http.delete<any>(`${this.url}/${id}`, httpOptions).pipe(
    retry(2),
    tap(() => {
        this.refreshServiceNeeded.next();
    })
  );
}
}
