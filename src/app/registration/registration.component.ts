import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DoctorService } from '../doctor/doctor.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  submitted:boolean=false;
  degree:String | undefined;
  RegistrationForm = this.fb.group({
    mno: new FormControl('',[Validators.required]),
    fname: new FormControl('',[Validators.required]),
    email: new FormControl('',[Validators.required, Validators.email]),
    password: new FormControl('',[Validators.required,Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$')]),
    confirmPassword:new FormControl('',[Validators.required]),
    date:new FormControl('',[Validators.required]),
    degree1:new FormControl('',[Validators.required]),
    degree: this.fb.array([
      this.fb.control('',[Validators.required])
    ]),
    onconfee:new FormControl('',[Validators.required]),
    fee2: new FormControl('',[Validators.required]),
  },
   {
    validator: this.MustMatch('password', 'confirmPassword')
  });
 
  constructor( private router: Router,
    private doctorService: DoctorService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  get f() { return this.RegistrationForm.controls; } 

  get fname() {
    return this.RegistrationForm.get('fname');
  }

  get confee() {
    return this.RegistrationForm.get('confee');
  }

  get fee2() {
    return this.RegistrationForm.get('fee2');
  }

  onSubmit(){
    this.submitted=true;
    this.degree="";
    console.log(this.RegistrationForm.value);
    this.router.navigateByUrl('registration2');
    this.degree=this.degree+this.RegistrationForm.value.degree1 
    
    console.log(this.degree);
    this.degree=this.degree+","+this.RegistrationForm.value.degree
    console.log(this.degree);
    // this.doctorService.doctors.push({degree:this.degree,
    // fname:this.RegistrationForm.value.fname,experiance:"12.5",
    // photo:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHMArQMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAACAwEEAAUGB//EADoQAAIBAwMDAgQDBgMJAAAAAAECAAMEEQUSIQYxQRNRImGBkQcU0SNCcaGx8DJiwRUWJUNScoKi4f/EABkBAQADAQEAAAAAAAAAAAAAAAEAAgQDBf/EACIRAQEAAwABAwUBAAAAAAAAAAABAgMREgQhMRQiMkFRYf/aAAwDAQACEQMRAD8AZTEeiwKYxLCCei8wSLHKvEhBGqIFAEICSBCAgUhZIHMJRC25gQgSdsnGJIk6Q4kPtRSzEAAZJPiNnHfibVuk0e2pWjsPzFwKTKv72QcA/LIlMsuTq2GPllIzUOs6KVSthSo1aQOPVr1DTV/+3g5jNP6wtbh0W5FOmH4Dq+Rn6yen/wAOqV9aUH12uKlRRytEEcexPmbi/wDw10GnbOltTqK5HwM1QnaZg+qy78vS+jx58LSMlVA9NldT2KnIMwrOc6IoVrM6hYu5ZKFQbVznBOc4+06dhPQwy88evN2YeGVxIKxbLLGIDCXUU3SIqU8y8yxNReIq1rqiSuy8y+6yuycxC2glhFikEeg5kQxRGLAENTKrDEISFkiQjEISFhQLJOJEkGCJnPdchBoqVGUs6XNJqYXvuz+mZ0OZQ1yk1fTKuwAvSIqqD52ndj7CUz741012ec613SnVeq3Go0LKroxpW1Q7BX352n/MDzNjUver62ovSaytPym7CEqeV985xNRddV3rVtPWysDWYkMKRUqHA8qwB5m8pa3q6BfUsmXBzVwMJTT5k+Z5Ne9Of1R0vTa9nrOq1mT9lWemdoYfC20k/TJM2hETp9490LwttKm4OGA5OFUfaWMcT0/TSzXOvG9XcbtviWRAZY0iA00MpLCJcSwRFssgUqixDLzLtQSuRzLKmqI5RFoI4QSCAhrAAjFgRiSJAhZkWSDCBgSCYIZmDuiy0EvJxDt8zfK3qZMuWFpWuqqbVPplwrN47wvsZLbyOJtb6x0HVbzSL64P5ZaguLRmGRTLclD7Y8Taat1DT1ayttH0J3r3ldhvZR8KqO+ZS6+6a06wqVb3ULpKX7XaBUBJrZ5wNvORn9Z2f4e6No9jpdK/0+tRuPzGMVV5we233BzwR3mHPR9/+PVw9RzXyit9AfR9Ioop3hOKh+Z8n6xJne/l0ZGpuoYMMNmaW86dRiWtKmz/ACPyPv3/AKzRhnJOMWzXbfKObMBhLV3Z17VttemV9j3B+srsJ3jPZz5KMWw4jTAIiqruMxBGDLTyu3eKok7RoikjRIggYaGAMQxITJkEGTmBTmCzTDFswxIUM0WW9pjNLmgW/wCY1SmCAQnxbff++8L7Tpk7eN3pWg00pGveAVW2bgn7o/XzN29r6TKKSotKmM7QPMhT6dK5TnhNwz/fyltuagx5HHzmXLK1txxmM9nnf4x2dlT6Zq3z0wbio9GnSY+CXXOPbsT9Jufw20qyTp3StQpWyUqz2dIPsJCu4QKXI7bu4z38Sl+JmhXPUlLp/TLcstN9R/buo/wUwjEn+RxnzidvZ0KNpa0rW0pinbUUFNAOwAGAB+sLlTw5g/xbamM+65xC8wE7Y+cgvyOfJgRVKaVaZSqiup7hhmcTrFoLK9emowh+JP4H+zO4ByM+JzPVNMM1O47DPpj5+f1nTVeZOO6dx650wSIZgtNTITUldhzLD94k94xUKGOHIldY5eJLAMQwYAhduYEeZGZEiCyd3EW5mE4iqjcSIB2xOl6Qt3CVbgqGV+APcDv9eZzFFDcXNOiDt3tjcfE6jUKlzp7UbO0RqVBVGxlqbWq/cYJ+onD1GyYYtPpdV2ZumuqXwNVByTSZCfcHkf385ltU30kVu4AIM0djqFw6PTqVC6Zx8fDqfYg/18/zjLLUglzsq/Dzz8pmw2TP4bc9OWDdVBlt+cJnke8fkcccCAm2tSdARznBmrvauoiraUrBKYWq2LitUbmioByVXy2cDnidHJHVutf7v9PX2rInqNb0iVpn99jwo++PpPPrm+1Tp2yTX7vqelfXY2Nd2j1QUbcRupog4BHuOeJ1XV2gUL6xo29owWpUqqapqVWJqqDk7/f/AOcTmdQ0PU+oNZsNGu7QWui23p1KtSmQRXCBfgGPmD44yTCZTvFvG869Ut3FWgj54Zd30M0nVa5s6TdlFUBfqCT/AKTfIoCKMYUeJzXVlwWejbgfAuX/AIntL6/ycdv4Vz8BjCMBprYgNEt3jXMQx5jFS1McrSukapjQeDJgKYUEFIJgmCxMCxiJWqt7Q3fiVKzyFuelESpqZdk3uijYM8Ak4z/Wd7Vp0Lm39GvTVkPOJ5f0/qC2Oqo1Q/C4K5z2J7H+/edw+oUqOxN+6ow+FB3/AIn5TPtx7WvRlzFfOkUtytTqlQvABGcD2i7nQkrVVcVccYOV7xH+06S4apXRCOCC3iQ2v2dMHFdfq3E4TRJ7yNV35futpb2n5GgT6/wICSW9pROpVaqlqNAJk7vi5/lNTr3UYr6dVttPb07hyoSo+Cp5BwB5yBic/t6z9Naqm1KcqN74z9hjMrsx2fpbXlr+cnSuxarufPLgkky9Zhkq09pbk4IznOZ5hrOp9V1fz1hUp2Vk1ram5eo/xEgeB/HB5+XMqdE39/p/VNtR125va1Q0XdQa/FQkbkZFyAPhyMHzOWOnKXtdct+Nx5HvBb4MHg+ROL6jq79SI/6VAnWpWU0PUz3XdjseROB1Cv619WfPBbibtU93nb79oO8g9pCmQ5mhjA5iG7w2Jij3loCUMcplZDHKYg9TDzxEqYzMCkmLcwiYlzBC6jYlKs/ePrMJQrv3gitcVMHgxKanVtyf3we+WIJ+ogXDzWV35hV5bGwbqq1Ff0atvdBs4ylYMPsQP6yzSv7y7JXT7O4OMZLkALntxzF6boHrV7OtUUHfVTuucjM9RtdOo2mrVFRFAakoIx3wZytrVjHEWmm65Y3NO+rWAu2pHIK1DlffCn9ZtV65oUKrFqF5Y1HP7SnWolqbHzxj/Wegemq0yMfeUb2ws7qkUuKCuD4x3lJlL8r+P8ec6xf6frdcV7euaFX0jRY0XDKUPjaw7Z+3gic6miigxIqJcVSU23Nd1Z1CjC4z2AB8Y8e07nVOhbWoS1rbJR+ScGcpc9D39OoxG7b4yJeeKtmTttL1taPT9O3qXLXN0oILseST54lFXycma7StLqafppaou1vUHb5y4jzrhJz2ZttveVaDcSGaKDzC0s5JYxLNzCZopjzLAilHLMmSA0Q/EyZIQExVQnEyZCop1icTX3BPMyZAtdcGa2v/AIpkyVq8el6Mo9LThj/nqP8A2E6y5P8AxlV4wQBjHiTMnHJsx+G0rAKoAAA+UFgDT5HmZMnN0ZVUYY45lW4RTRyR4mTJIK0OtIq6NWZVAPqLz/5Cc4hmTJp1/DFv/IzMgmZMnRxQ0WSczJkQ/9k=",
    // online:this.RegistrationForm.value.onconfee,offline:this.RegistrationForm.value.fee2});
    let payload = {
        // degree:this.degree.value,
        // fname:this.RegistrationForm.value.fname,
        // experiance:"12.5",
        // photo:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHMArQMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAACAwEEAAUGB//EADoQAAIBAwMDAgQDBgMJAAAAAAECAAMEEQUSIQYxQRNRImGBkQcU0SNCcaGx8DJiwRUWJUNScoKi4f/EABkBAQADAQEAAAAAAAAAAAAAAAEAAgQDBf/EACIRAQEAAwABAwUBAAAAAAAAAAABAgMREgQhMRQiMkFRYf/aAAwDAQACEQMRAD8AZTEeiwKYxLCCei8wSLHKvEhBGqIFAEICSBCAgUhZIHMJRC25gQgSdsnGJIk6Q4kPtRSzEAAZJPiNnHfibVuk0e2pWjsPzFwKTKv72QcA/LIlMsuTq2GPllIzUOs6KVSthSo1aQOPVr1DTV/+3g5jNP6wtbh0W5FOmH4Dq+Rn6yen/wAOqV9aUH12uKlRRytEEcexPmbi/wDw10GnbOltTqK5HwM1QnaZg+qy78vS+jx58LSMlVA9NldT2KnIMwrOc6IoVrM6hYu5ZKFQbVznBOc4+06dhPQwy88evN2YeGVxIKxbLLGIDCXUU3SIqU8y8yxNReIq1rqiSuy8y+6yuycxC2glhFikEeg5kQxRGLAENTKrDEISFkiQjEISFhQLJOJEkGCJnPdchBoqVGUs6XNJqYXvuz+mZ0OZQ1yk1fTKuwAvSIqqD52ndj7CUz741012ec613SnVeq3Go0LKroxpW1Q7BX352n/MDzNjUver62ovSaytPym7CEqeV985xNRddV3rVtPWysDWYkMKRUqHA8qwB5m8pa3q6BfUsmXBzVwMJTT5k+Z5Ne9Of1R0vTa9nrOq1mT9lWemdoYfC20k/TJM2hETp9490LwttKm4OGA5OFUfaWMcT0/TSzXOvG9XcbtviWRAZY0iA00MpLCJcSwRFssgUqixDLzLtQSuRzLKmqI5RFoI4QSCAhrAAjFgRiSJAhZkWSDCBgSCYIZmDuiy0EvJxDt8zfK3qZMuWFpWuqqbVPplwrN47wvsZLbyOJtb6x0HVbzSL64P5ZaguLRmGRTLclD7Y8Taat1DT1ayttH0J3r3ldhvZR8KqO+ZS6+6a06wqVb3ULpKX7XaBUBJrZ5wNvORn9Z2f4e6No9jpdK/0+tRuPzGMVV5we233BzwR3mHPR9/+PVw9RzXyit9AfR9Ioop3hOKh+Z8n6xJne/l0ZGpuoYMMNmaW86dRiWtKmz/ACPyPv3/AKzRhnJOMWzXbfKObMBhLV3Z17VttemV9j3B+srsJ3jPZz5KMWw4jTAIiqruMxBGDLTyu3eKok7RoikjRIggYaGAMQxITJkEGTmBTmCzTDFswxIUM0WW9pjNLmgW/wCY1SmCAQnxbff++8L7Tpk7eN3pWg00pGveAVW2bgn7o/XzN29r6TKKSotKmM7QPMhT6dK5TnhNwz/fyltuagx5HHzmXLK1txxmM9nnf4x2dlT6Zq3z0wbio9GnSY+CXXOPbsT9Jufw20qyTp3StQpWyUqz2dIPsJCu4QKXI7bu4z38Sl+JmhXPUlLp/TLcstN9R/buo/wUwjEn+RxnzidvZ0KNpa0rW0pinbUUFNAOwAGAB+sLlTw5g/xbamM+65xC8wE7Y+cgvyOfJgRVKaVaZSqiup7hhmcTrFoLK9emowh+JP4H+zO4ByM+JzPVNMM1O47DPpj5+f1nTVeZOO6dx650wSIZgtNTITUldhzLD94k94xUKGOHIldY5eJLAMQwYAhduYEeZGZEiCyd3EW5mE4iqjcSIB2xOl6Qt3CVbgqGV+APcDv9eZzFFDcXNOiDt3tjcfE6jUKlzp7UbO0RqVBVGxlqbWq/cYJ+onD1GyYYtPpdV2ZumuqXwNVByTSZCfcHkf385ltU30kVu4AIM0djqFw6PTqVC6Zx8fDqfYg/18/zjLLUglzsq/Dzz8pmw2TP4bc9OWDdVBlt+cJnke8fkcccCAm2tSdARznBmrvauoiraUrBKYWq2LitUbmioByVXy2cDnidHJHVutf7v9PX2rInqNb0iVpn99jwo++PpPPrm+1Tp2yTX7vqelfXY2Nd2j1QUbcRupog4BHuOeJ1XV2gUL6xo29owWpUqqapqVWJqqDk7/f/AOcTmdQ0PU+oNZsNGu7QWui23p1KtSmQRXCBfgGPmD44yTCZTvFvG869Ut3FWgj54Zd30M0nVa5s6TdlFUBfqCT/AKTfIoCKMYUeJzXVlwWejbgfAuX/AIntL6/ycdv4Vz8BjCMBprYgNEt3jXMQx5jFS1McrSukapjQeDJgKYUEFIJgmCxMCxiJWqt7Q3fiVKzyFuelESpqZdk3uijYM8Ak4z/Wd7Vp0Lm39GvTVkPOJ5f0/qC2Oqo1Q/C4K5z2J7H+/edw+oUqOxN+6ow+FB3/AIn5TPtx7WvRlzFfOkUtytTqlQvABGcD2i7nQkrVVcVccYOV7xH+06S4apXRCOCC3iQ2v2dMHFdfq3E4TRJ7yNV35futpb2n5GgT6/wICSW9pROpVaqlqNAJk7vi5/lNTr3UYr6dVttPb07hyoSo+Cp5BwB5yBic/t6z9Naqm1KcqN74z9hjMrsx2fpbXlr+cnSuxarufPLgkky9Zhkq09pbk4IznOZ5hrOp9V1fz1hUp2Vk1ram5eo/xEgeB/HB5+XMqdE39/p/VNtR125va1Q0XdQa/FQkbkZFyAPhyMHzOWOnKXtdct+Nx5HvBb4MHg+ROL6jq79SI/6VAnWpWU0PUz3XdjseROB1Cv619WfPBbibtU93nb79oO8g9pCmQ5mhjA5iG7w2Jij3loCUMcplZDHKYg9TDzxEqYzMCkmLcwiYlzBC6jYlKs/ePrMJQrv3gitcVMHgxKanVtyf3we+WIJ+ogXDzWV35hV5bGwbqq1Ff0atvdBs4ylYMPsQP6yzSv7y7JXT7O4OMZLkALntxzF6boHrV7OtUUHfVTuucjM9RtdOo2mrVFRFAakoIx3wZytrVjHEWmm65Y3NO+rWAu2pHIK1DlffCn9ZtV65oUKrFqF5Y1HP7SnWolqbHzxj/Wegemq0yMfeUb2ws7qkUuKCuD4x3lJlL8r+P8ec6xf6frdcV7euaFX0jRY0XDKUPjaw7Z+3gic6miigxIqJcVSU23Nd1Z1CjC4z2AB8Y8e07nVOhbWoS1rbJR+ScGcpc9D39OoxG7b4yJeeKtmTttL1taPT9O3qXLXN0oILseST54lFXycma7StLqafppaou1vUHb5y4jzrhJz2ZttveVaDcSGaKDzC0s5JYxLNzCZopjzLAilHLMmSA0Q/EyZIQExVQnEyZCop1icTX3BPMyZAtdcGa2v/AIpkyVq8el6Mo9LThj/nqP8A2E6y5P8AxlV4wQBjHiTMnHJsx+G0rAKoAAA+UFgDT5HmZMnN0ZVUYY45lW4RTRyR4mTJIK0OtIq6NWZVAPqLz/5Cc4hmTJp1/DFv/IzMgmZMnRxQ0WSczJkQ/9k=",
        // online:this.RegistrationForm.value.onconfee,
        // offline:this.RegistrationForm.value.fee2
        
              degree:this.degree,
              fname:this.RegistrationForm.value.fname,
              experiance:"12.5",
              photo:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHMArQMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAACAwEEAAUGB//EADoQAAIBAwMDAgQDBgMJAAAAAAECAAMEEQUSIQYxQRNRImGBkQcU0SNCcaGx8DJiwRUWJUNScoKi4f/EABkBAQADAQEAAAAAAAAAAAAAAAEAAgQDBf/EACIRAQEAAwABAwUBAAAAAAAAAAABAgMREgQhMRQiMkFRYf/aAAwDAQACEQMRAD8AZTEeiwKYxLCCei8wSLHKvEhBGqIFAEICSBCAgUhZIHMJRC25gQgSdsnGJIk6Q4kPtRSzEAAZJPiNnHfibVuk0e2pWjsPzFwKTKv72QcA/LIlMsuTq2GPllIzUOs6KVSthSo1aQOPVr1DTV/+3g5jNP6wtbh0W5FOmH4Dq+Rn6yen/wAOqV9aUH12uKlRRytEEcexPmbi/wDw10GnbOltTqK5HwM1QnaZg+qy78vS+jx58LSMlVA9NldT2KnIMwrOc6IoVrM6hYu5ZKFQbVznBOc4+06dhPQwy88evN2YeGVxIKxbLLGIDCXUU3SIqU8y8yxNReIq1rqiSuy8y+6yuycxC2glhFikEeg5kQxRGLAENTKrDEISFkiQjEISFhQLJOJEkGCJnPdchBoqVGUs6XNJqYXvuz+mZ0OZQ1yk1fTKuwAvSIqqD52ndj7CUz741012ec613SnVeq3Go0LKroxpW1Q7BX352n/MDzNjUver62ovSaytPym7CEqeV985xNRddV3rVtPWysDWYkMKRUqHA8qwB5m8pa3q6BfUsmXBzVwMJTT5k+Z5Ne9Of1R0vTa9nrOq1mT9lWemdoYfC20k/TJM2hETp9490LwttKm4OGA5OFUfaWMcT0/TSzXOvG9XcbtviWRAZY0iA00MpLCJcSwRFssgUqixDLzLtQSuRzLKmqI5RFoI4QSCAhrAAjFgRiSJAhZkWSDCBgSCYIZmDuiy0EvJxDt8zfK3qZMuWFpWuqqbVPplwrN47wvsZLbyOJtb6x0HVbzSL64P5ZaguLRmGRTLclD7Y8Taat1DT1ayttH0J3r3ldhvZR8KqO+ZS6+6a06wqVb3ULpKX7XaBUBJrZ5wNvORn9Z2f4e6No9jpdK/0+tRuPzGMVV5we233BzwR3mHPR9/+PVw9RzXyit9AfR9Ioop3hOKh+Z8n6xJne/l0ZGpuoYMMNmaW86dRiWtKmz/ACPyPv3/AKzRhnJOMWzXbfKObMBhLV3Z17VttemV9j3B+srsJ3jPZz5KMWw4jTAIiqruMxBGDLTyu3eKok7RoikjRIggYaGAMQxITJkEGTmBTmCzTDFswxIUM0WW9pjNLmgW/wCY1SmCAQnxbff++8L7Tpk7eN3pWg00pGveAVW2bgn7o/XzN29r6TKKSotKmM7QPMhT6dK5TnhNwz/fyltuagx5HHzmXLK1txxmM9nnf4x2dlT6Zq3z0wbio9GnSY+CXXOPbsT9Jufw20qyTp3StQpWyUqz2dIPsJCu4QKXI7bu4z38Sl+JmhXPUlLp/TLcstN9R/buo/wUwjEn+RxnzidvZ0KNpa0rW0pinbUUFNAOwAGAB+sLlTw5g/xbamM+65xC8wE7Y+cgvyOfJgRVKaVaZSqiup7hhmcTrFoLK9emowh+JP4H+zO4ByM+JzPVNMM1O47DPpj5+f1nTVeZOO6dx650wSIZgtNTITUldhzLD94k94xUKGOHIldY5eJLAMQwYAhduYEeZGZEiCyd3EW5mE4iqjcSIB2xOl6Qt3CVbgqGV+APcDv9eZzFFDcXNOiDt3tjcfE6jUKlzp7UbO0RqVBVGxlqbWq/cYJ+onD1GyYYtPpdV2ZumuqXwNVByTSZCfcHkf385ltU30kVu4AIM0djqFw6PTqVC6Zx8fDqfYg/18/zjLLUglzsq/Dzz8pmw2TP4bc9OWDdVBlt+cJnke8fkcccCAm2tSdARznBmrvauoiraUrBKYWq2LitUbmioByVXy2cDnidHJHVutf7v9PX2rInqNb0iVpn99jwo++PpPPrm+1Tp2yTX7vqelfXY2Nd2j1QUbcRupog4BHuOeJ1XV2gUL6xo29owWpUqqapqVWJqqDk7/f/AOcTmdQ0PU+oNZsNGu7QWui23p1KtSmQRXCBfgGPmD44yTCZTvFvG869Ut3FWgj54Zd30M0nVa5s6TdlFUBfqCT/AKTfIoCKMYUeJzXVlwWejbgfAuX/AIntL6/ycdv4Vz8BjCMBprYgNEt3jXMQx5jFS1McrSukapjQeDJgKYUEFIJgmCxMCxiJWqt7Q3fiVKzyFuelESpqZdk3uijYM8Ak4z/Wd7Vp0Lm39GvTVkPOJ5f0/qC2Oqo1Q/C4K5z2J7H+/edw+oUqOxN+6ow+FB3/AIn5TPtx7WvRlzFfOkUtytTqlQvABGcD2i7nQkrVVcVccYOV7xH+06S4apXRCOCC3iQ2v2dMHFdfq3E4TRJ7yNV35futpb2n5GgT6/wICSW9pROpVaqlqNAJk7vi5/lNTr3UYr6dVttPb07hyoSo+Cp5BwB5yBic/t6z9Naqm1KcqN74z9hjMrsx2fpbXlr+cnSuxarufPLgkky9Zhkq09pbk4IznOZ5hrOp9V1fz1hUp2Vk1ram5eo/xEgeB/HB5+XMqdE39/p/VNtR125va1Q0XdQa/FQkbkZFyAPhyMHzOWOnKXtdct+Nx5HvBb4MHg+ROL6jq79SI/6VAnWpWU0PUz3XdjseROB1Cv619WfPBbibtU93nb79oO8g9pCmQ5mhjA5iG7w2Jij3loCUMcplZDHKYg9TDzxEqYzMCkmLcwiYlzBC6jYlKs/ePrMJQrv3gitcVMHgxKanVtyf3we+WIJ+ogXDzWV35hV5bGwbqq1Ff0atvdBs4ylYMPsQP6yzSv7y7JXT7O4OMZLkALntxzF6boHrV7OtUUHfVTuucjM9RtdOo2mrVFRFAakoIx3wZytrVjHEWmm65Y3NO+rWAu2pHIK1DlffCn9ZtV65oUKrFqF5Y1HP7SnWolqbHzxj/Wegemq0yMfeUb2ws7qkUuKCuD4x3lJlL8r+P8ec6xf6frdcV7euaFX0jRY0XDKUPjaw7Z+3gic6miigxIqJcVSU23Nd1Z1CjC4z2AB8Y8e07nVOhbWoS1rbJR+ScGcpc9D39OoxG7b4yJeeKtmTttL1taPT9O3qXLXN0oILseST54lFXycma7StLqafppaou1vUHb5y4jzrhJz2ZttveVaDcSGaKDzC0s5JYxLNzCZopjzLAilHLMmSA0Q/EyZIQExVQnEyZCop1icTX3BPMyZAtdcGa2v/AIpkyVq8el6Mo9LThj/nqP8A2E6y5P8AxlV4wQBjHiTMnHJsx+G0rAKoAAA+UFgDT5HmZMnN0ZVUYY45lW4RTRyR4mTJIK0OtIq6NWZVAPqLz/5Cc4hmTJp1/DFv/IzMgmZMnRxQ0WSczJkQ/9k=",
              online:this.RegistrationForm.value.onconfee,
              offline:this.RegistrationForm.value.fee2,
              id: this.generateUUID()
  };

  this.doctorService
      .postData(payload)
      .toPromise()
      .then((response: any) => {     
        console.log(response);   
      })
  }

  get addDegree() {
    return this.RegistrationForm.get('degree') as FormArray;
  }

  private generateUUID() {
    var d = new Date().getTime(); //Timestamp
    var d2 = (typeof performance !== 'undefined' && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16; //random number between 0 and 16
        if (d > 0) {
            //Use timestamp until depleted
            r = (d + r) % 16 | 0;
            d = Math.floor(d / 16);
        } else {
            //Use microseconds since page-load if supported
            r = (d2 + r) % 16 | 0;
            d2 = Math.floor(d2 / 16);
        }
        return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });
}

  onAddDegree() {
    const control = new FormControl(null,[Validators.required]);
    // (<FormArray>this.myReactiveForm.get('skills')).push(control)
    this.addDegree.push(control);
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
    return;
    }
    if (control.value !== matchingControl.value) {
    matchingControl.setErrors({ mustMatch: true });
    } else {
    matchingControl.setErrors(null);
    }
    }
    }
}
