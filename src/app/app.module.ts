import { NgModule } from '@angular/core';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { Registration2Component } from './registration2/registration2.component';
import { RegistrationDoctorComponent } from './registration-doctor/registration-doctor.component';
import { ViewDoctorComponent } from './view-doctor/view-doctor.component';
import { DoctorService } from './doctor/doctor.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    Registration2Component,
    RegistrationDoctorComponent,
    ViewDoctorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule ,
    HttpClientModule,
  ],
  providers: [DoctorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
