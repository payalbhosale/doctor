import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorService } from '../doctor/doctor.service';

@Component({
  selector: 'app-view-doctor',
  templateUrl: './view-doctor.component.html',
  styleUrls: ['./view-doctor.component.css']
})
export class ViewDoctorComponent implements OnInit {
  doctors:any | undefined;
  preLength:number=3;
  newLength=10;
  ApiData=[];
  loadMoreBtn: boolean=false;
  constructor(private doctorService: DoctorService,
    private router: Router,
    private DoctorService: DoctorService) { }

  ngOnInit(){
    // this.data=this.doctorService.doctors  
    this.getData();  
  }
  getData(){
    this.DoctorService.getData().subscribe(
      (data) => {
        this.ApiData=data
        console.log("data",data)
        console.log("this.ApiData",this.ApiData)
        this.doctors = this.ApiData.slice(0, this.newLength);
        console.log("this.ApiData++",this.ApiData)
       console.log("***",this.doctors);
      });
  }

  login(){ 
  }
  

  Gotohome(){
    this.router.navigateByUrl('');
  }

  removeDoctor(id:any){
    // this.doctorService.doctors.splice(i,1)
    this.doctorService.removeData(id).subscribe((res) => {
      console.log("Remove")
  });
  this.getData()
  }

  loadMore(){
    let prev=this.newLength;
    this.newLength = this.newLength + 3;
    console.log(this.newLength)
    console.log(this.doctors.length)
    if (prev < this.ApiData.length) {
     this.doctors = this.ApiData.slice(prev, this.newLength);
    }
    else{
      this.loadMoreBtn=true;
    }
  }
}
