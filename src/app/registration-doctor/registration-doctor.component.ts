import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-doctor',
  templateUrl: './registration-doctor.component.html',
  styleUrls: ['./registration-doctor.component.css']
})
export class RegistrationDoctorComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  viewDoctors(){
    this.router.navigateByUrl('viewdoctor');
  }

  login(){}

  register(){
    this.router.navigateByUrl('registration');
  }
}
