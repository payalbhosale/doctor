import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationDoctorComponent } from './registration-doctor/registration-doctor.component';
import { RegistrationComponent } from './registration/registration.component';
import { Registration2Component } from './registration2/registration2.component';
import { ViewDoctorComponent } from './view-doctor/view-doctor.component';

const routes: Routes = [
  { path: 'registration2', component: Registration2Component},
  {  path: '', component: RegistrationDoctorComponent},
  {  path: 'registration', component: RegistrationComponent},
  {  path: 'viewdoctor', component:ViewDoctorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
